#!/bin/bash

# © Outsourcing IT - Konopnickiej.Com
# Author: Paweł Wojciechowski

# RUBY_VER="2.5.3"
# Always latest stable Ruby version is going to be installed (check app/app.sh)

# Uncomment below line if you need specific Ruby on Rails version.
# Otherwise latest, stable Ruby on Rails will be installed.
# gem install rails

#RUBY_ON_RAILS_VER="4.2.5.1"
